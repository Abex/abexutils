// ==UserScript==
// @name        Better Sparknotes
// @namespace   abex
// @include     http://www.sparknotes.com/**html
// @version     1
// @grant       none
// ==/UserScript==

document.getElementsByClassName("col-1-3 last")[1].outerHTML="";
if((document.getElementsByClassName("col-mid")[0]))
  document.getElementsByClassName("col-mid")[0].style.width="calc(100% - 235px)";
else
{
  document.getElementsByClassName("NFS-container")[0].style.width="90%";
  document.getElementsByClassName("noFearLit")[0].style.width="100%";
}