// ==UserScript==
// @name        XKCD Betterifyer
// @namespace   Abex
// @include     http*://*xkcd.com/*
// @grant       none
// ==/UserScript==

try{//some are not images
  console.log("Starting");
  var e = document.createElement("div");
  e.innerHTML = ($("#comic > img")[0].title);
  //console.log(e);
  $("#comic")[0].appendChild(e);
}catch(e){console.log(e);}
function findButton(contains)
{
  var t = document.getElementsByTagName("a");
  //console.log(t);
  for(var i=0;i<t.length;i++){
    //console.log(t[i]);
    if(t[i].innerHTML.indexOf(contains)!=-1)
      t[i].click();
  }
}
document.body.addEventListener("keypress",function(e)
{
  switch(e.keyCode||e.charCode)
  {
    case 97:// "a"
    case 37:// LeftArrow
      findButton("Prev");
    break;
    case 101:// "e" (dvorak "d")
    case 100:// "d"
    case 39://Right Arrow
      findButton("Next");
    break;
    case 111: //"o" (dvorak "s")
    case 115: //"s"
    case 40: // DownArrow
      findButton("Random");
    break;
  }
  //console.log(e);
});