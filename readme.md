Abex Utils
==========

Various utilities that deserve git, but not their own Repo.

Greasemonkey Scripts
--------------------
- [XKCD Betterifyer](https://bitbucket.org/Abex/abexutils/raw/master/Greasemonkey/xkcdBetterifyer.user.js) [[src](https://bitbucket.org/Abex/abexutils/src/master/Greasemonkey/xkcdBetterifyer.user.js?at=master)]
    - Shows alt text below image & Adds keyboard navigation
- [Better Sparknotes](https://bitbucket.org/Abex/abexutils/raw/master/Greasemonkey/BetterSparknotes.user.js) [[src](https://bitbucket.org/Abex/abexutils/src/master/Greasemonkey/BetterSparknotes.user.js?at=master)]
    - Removes the 'Sparklife' sidebar
- [ExternalNewLinks](https://bitbucket.org/Abex/abexutils/raw/master/Greasemonkey/ExternalNewLinks.user.js) [[src](https://bitbucket.org/Abex/abexutils/src/master/Greasemonkey/ExternalNewLinks.user.js?at=master)]
    - Opens external links in a new tab. (change includes for your sites)

C++
---
- [Qs](https://bitbucket.org/Abex/abexutils/raw/master/C++/Qs/)
    - A lightweight QString-like string formatter.
