var fs   = require("fs");
var path = require("path");
var mkdirpSync = require("mkdirp").sync;

var out = path.join(__dirname,"out.json");
var outDir = "D:\\newMusic\\";
var basedir = "D:\\Music\\";

var files = JSON.parse(fs.readFileSync(out));

var uniqueNames = {};

console.log(files[5893]);

var finalFiles = [];

console.log(files.length + " songs found");
files.forEach(function(e,i) {
  if(uniqueNames[e.data.title + " // " + e.data.artist.join(", ")])
    uniqueNames[e.data.title + " // " + e.data.artist.join(", ")][uniqueNames[e.data.title + " // " + e.data.artist.join(", ")].length] = i;
  else
    uniqueNames[e.data.title + " // " + e.data.artist.join(", ")] = [i];
});
console.log(Object.keys(uniqueNames).length + " Unique songs");
Object.keys(uniqueNames).forEach(function(title,i){
  var songIds = uniqueNames[title];
  songIds.sort(function(a,b)
  {
    if(files[a].data.duration<files[b].data.duration)
      return 1;
    else if(files[a].data.duration>files[b].data.duration)
      return -1;
    else
    return 0;
  });
  var i=0;
  if(files[songIds[i]].data.album.indexOf("Extended")!=-1)
  {
    while(songIds[i]!=undefined && files[songIds[i]].data.album.indexOf("Extended")!=-1)
      i++;
    if(songIds[i]!=undefined) finalFiles[finalFiles.length] = files[songIds[0]];
  }
  finalFiles[finalFiles.length] = files[songIds[0]];
});
console.log(finalFiles.length + "Songs chosen to keep");

var wordLists = {};
finalFiles.forEach(function(song)
{
  song.data.title.split(" ").forEach(function(part)
  {
    part = part.replace("(","");
    part = part.replace(")","");
    wordLists[part] = (wordLists[part]||0)+1;
  });
});
var k = Object.keys(wordLists);
k.sort(function(a,b)
{
  if(wordLists[a]<wordLists[b]) return -1;
  if(wordLists[a]>wordLists[b]) return 1;
  return 0;
});
k.forEach(function(e)
{
  console.log(e," - ", wordLists[e]);
});
