var fs   = require("fs");
var path = require("path");
var mkdirpSync = require("mkdirp").sync;

var out = path.join(__dirname,"out.json");
var outDir = "D:\\newMusic\\";
var basedir = "D:\\Music\\";

var files = JSON.parse(fs.readFileSync(out));

var uniqueNames = {};

console.log(files[5893]);

var finalFiles = [];

console.log(files.length + " songs found");
files.forEach(function(e,i) {
  if(uniqueNames[e.data.title + " // " + e.data.artist.join(", ")])
    uniqueNames[e.data.title + " // " + e.data.artist.join(", ")][uniqueNames[e.data.title + " // " + e.data.artist.join(", ")].length] = i;
  else
    uniqueNames[e.data.title + " // " + e.data.artist.join(", ")] = [i];
});
console.log(Object.keys(uniqueNames).length + " Unique songs");
Object.keys(uniqueNames).forEach(function(title,i){
  var songIds = uniqueNames[title];
  songIds.sort(function(a,b)
  {
    if(files[a].data.duration<files[b].data.duration)
      return 1;
    else if(files[a].data.duration>files[b].data.duration)
      return -1;
    else
    return 0;
  });
  var i=0;
  if(files[songIds[i]].data.album.indexOf("Extended")!=-1)
  {
    while(songIds[i]!=undefined && files[songIds[i]].data.album.indexOf("Extended")!=-1)
      i++;
    if(songIds[i]!=undefined) finalFiles[finalFiles.length] = files[songIds[0]];
  }
  finalFiles[finalFiles.length] = files[songIds[0]];
});
console.log(finalFiles.length + "Songs chosen to keep");
finalFiles.forEach(function(song)
{
  var nPath = path.join(outDir,song.file.path);
  if(!fs.existsSync(path.dirname(nPath))) mkdirpSync(path.dirname(nPath));
  if(!fs.existsSync(nPath))
    fs.symlinkSync(song.file.fullPath,nPath,"file");
  else
    console.log(nPath + " exists!");
});
