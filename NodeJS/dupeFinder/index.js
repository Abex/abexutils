var id = require("musicmetadata");
var readdirp = require("readdirp");
var fs   = require("fs");
var path = require("path");

var basedir = "D:/Music/Super Eurobeat";
var out = path.join(__dirname,"out.json");

var files=[];
var f2=[];

console.log("listing files")
var stream = readdirp({root:basedir,fileFilter:["*.mp3","*.ogg","*.flac","*.wma"]});
stream
.on('warn',function(err){console.log("Warning:",err);})
.on('error',function(err){console.log("Error:",err);})
.on('data',function(entry){files[files.length]=entry;})
.on('end',function()
{
  function done(i)
  {
    if(i%10==0) console.log(i+"/"+files.length);
    if(i>=files.length) return wait();;
    var  ni = files[i];
    var rStream = fs.createReadStream(files[i].fullPath);
    var idStream = id(rStream,function(err,data)
    {
      if(err) console.log("Error - "+files[i].fullPath+err);
      data.picture=undefined;
      f2[f2.length]= {data:data,file:ni};
      rStream.close();
      done(++i);
    });
  }
  done(0);
  var wait = function()
  {
    console.log("writing!");
    fs.writeFileSync(out,"[");
    for(var i=0;i<f2.length;i++)
    {
      fs.appendFileSync(out,JSON.stringify(f2[i])+((f2.length-1>i)?",":""));
    }
    fs.appendFileSync(out,"]");
    console.log("done");
    process.exit();
  };
});
